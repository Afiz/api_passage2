# -*- coding: utf-8 -*-
import api_passage2.bdd_table.init_tables
import utils.json_utils
import logging
import traceback
from werkzeug.exceptions import BadRequest
import sys

from flask_restx import Resource, Namespace, fields, reqparse
api = Namespace('passage2', description="api d'accés à passage2")

# swagger retourne
reponse_ok = api.model('Success', {'ok': fields.Boolean})
reponse_ko = api.model('Error', {'erreur': fields.String})

# initialisation base de donnée
# cette fonction permet la creation ou la modification des tables utilisées
api_passage2.bdd_table.init_tables.init_tables()

# init du contenu du post
post_parser = reqparse.RequestParser()
post_parser.add_argument('Auth-User', location='json', type=str, required=True, help="Identifiant de l'utilisateur")
post_parser.add_argument('Auth-Roles', location='json', type=str, required=True, help="Rôle de l'utilisateur au sein de l'application")
post_parser.add_argument('Auth-Service', location='json', type=str, required=True, help="Service de l'utilisateur")
post_parser.add_argument('Auth-Name', location='json', type=str, required=True, help="Prénom et nom de l'utilisateur")
post_parser.add_argument('Auth-Email', location='json', type=str, required=True, help="E-mail de l'utilisateur")
post_parser.add_argument('Auth-Mode', location='json', type=str, required=True, help="Mode d'authentification")


@api.route('/login')
class LoginPassage2(Resource):
    """ Login avec Passage 2 """

    @api.response(200, "succès de la requête", reponse_ok)
    @api.response(500, "erreur serveur", reponse_ko)
    @api.expect(post_parser)
    def post(self):
        args = None
        try:
            # parse le json
            args = post_parser.parse_args()
            logging.info(args)

            # test la validité des champs json
            reponse = []
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-User', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-Roles', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-Service', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-Name', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-Email', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)
            resultat = utils.json_utils.test_key_good_format(args, 'Auth-Mode', str, vide_interdit=True)
            if resultat is not None:
                reponse.append(resultat)

            # si reponse est vide, c'est qu'il n'y a pas eu d'erreurs
            if len(reponse) == 0:
                profil_passage2 = api_passage2.bdd_table.table_profil_passage2.get_profil_passage2(args['Auth-User'])
                if len(profil_passage2) == 0:
                    api_passage2.bdd_table.table_profil_passage2.insert_profil_passage2(args)
                else:
                    api_passage2.bdd_table.table_profil_passage2.update_profil_passage2(args)
                reponse = {"ok": True}
                code_http = 200
            else:
                code_http = 400

        except BadRequest as e:
            code_http = e.code
            reponse = [e.data]
        except Exception as e:
            code_http = 500
            trace = traceback.format_exc()
            reponse = ["type=%s - value=%s - traceback=%s" % (sys.exc_info()[0], e, trace)]

        """
        Les traces conservées lors du login d'un utilisateur sont:
        En cas nominal :
            - id user
        En cas d'erreur :
            - Code http
            - erreur
        """
        if code_http >= 400:
            logging.error("codeHttp=%d - erreur='%s'" % (code_http, reponse))
            reponse = {"erreur": reponse}
        else:
            logging.info("login de l'utilisateur %s" % args['Auth-User'])
            global func_callback
            func_callback(reponse)

        return reponse, code_http


# callback
func_callback = None

def set_callback(callback):
    """
    initialise la fonction de callback
    :param callback: fonction de callback
    :return:
    """
    global func_callback
    func_callback = callback
