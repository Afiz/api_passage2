# -*- coding: utf-8 -*-
import bdd.connection
import bdd.table
import psycopg2.extras
import logging

table_name = "profil_passage2"


def delete_table():
    """
     effacement de la table
     :return: None si reussite sinon message d'erreur
    """
    return bdd.table.delete(table_name)


def create_table():
    """
     creation de la table

     colonne[col] : nom de la colonne
     colonne[type]: type colonne
     colonne[constraint]: type de contrainte si vide pas de contrainte sur la colonne

     :return: None si reussite sinon message d'erreur
    """

    colonne = [
        {"col": "Auth_User",
         "type": "text",
         "constraint": [{"nom":"UK_"+table_name+"_Auth_User", "sql": "UNIQUE(Auth_User)"}]},
        {"col": "Auth_Roles",
         "type": "text"},
        {"col": "Auth_Service",
         "type": "text"},
        {"col": "Auth_Name",
         "type": "text"},
        {"col": "Auth_Email",
         "type": "text"},
        {"col": "Auth_Mode",
         "type": "text"},
    ]

    return bdd.table.create(table_name, colonne)


def get_profil_passage2(auth_user):
    """
    retourne le profil passage2 d'un utilisateur
    :param auth_user: json contenant les informations sur l'utilisateur
    :return: Un json contenant les donnée de l'utilisateur si reussite
             sinon message d'erreur
    """
    sql = """select *
             from {table_name}
             where auth_user = %s""".format(table_name=table_name, auth_user=auth_user)
    data = (auth_user,)

    return bdd.table.select(sql, data)


def update_profil_passage2(profil_passage2):
    """
    mise à jour de l'utilisateur
    :param profil_passage2: json contenant les informations sur l'utilisateur
    :return: None si reussite sinon message d'erreur
    """
    data = {"Auth_User": profil_passage2["Auth-User"],
            "Auth_Roles": profil_passage2["Auth-Roles"],
            "Auth_Service": profil_passage2["Auth-Service"],
            "Auth_Name": profil_passage2["Auth-Name"],
            "Auth_Email": profil_passage2["Auth-Email"],
            "Auth_Mode": profil_passage2["Auth-Mode"]}

    condition = {"Auth_User": profil_passage2["Auth-User"]}

    return bdd.table.update(table_name, condition, data)


def insert_profil_passage2(profil_passage2):
    """
    creation d'un nouvel utilisateur
    :param profil_passage2:
    :return:
    """
    data = {"Auth_User": profil_passage2["Auth-User"],
            "Auth_Roles": profil_passage2["Auth-Roles"],
            "Auth_Service": profil_passage2["Auth-Service"],
            "Auth_Name": profil_passage2["Auth-Name"],
            "Auth_Email":  profil_passage2["Auth-Email"],
            "Auth_Mode":  profil_passage2["Auth-Mode"]}

    bdd.table.insert(table_name, data)
